from Socket import openSocket, sendMessage
from Initialize import joinRoom
from Read import getUser, getMessage
import string
import os

s = openSocket()
joinRoom(s)
readbuffer = ""

while True:
    readbuffer = readbuffer + s.recv(1024)
    temp = string.split(readbuffer, "\n")
    readbuffer = temp.pop()

    for line in temp:
        print(line)
        if "PING" in line:
            s.send(line.replace("PING", "PONG"))
        user = getUser(line)
        message = getMessage(line)
        print user + " typed :" + message
        if "!isbotactive" in message:
            sendMessage(s, "Bot is currently online!")
        if "!queue" in message:
            os.chdir("C:\Users\name\PycharmProjects\untitled3\cache")
            with open('Queue.txt', 'r') as f:
                f_contents = f.read()
                sendMessage(s, "@" + user + " : the current queue is: " + f_contents)
            os.chdir("C:\Users\name\PycharmProjects\untitled3")
        if "!join" in message:
            os.chdir("C:\Users\name\PycharmProjects\untitled3\cache")
            with open('Queue.txt', 'a') as f:
                f.write(user + ", ")
            os.chdir("C:\Users\name\PycharmProjects\untitled3")
        if "!next" in message:
            os.chdir("C:\Users\name\PycharmProjects\untitled3\cache")

            os.chdir("C:\Users\name\PycharmProjects\untitled3")